import unittest
from io import StringIO
from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_solve


class TestDiplomacy(unittest.TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(['A Madrid Move London', 'B London Move Madrid'])
        self.assertEqual(v, ['A London', 'B Madrid'])

    def test_eval_2(self):
        v = diplomacy_eval([
            'A Madrid Hold',
            'B Barcelona Move Madrid',
            'C London Support B',
        ])
        self.assertEqual(v, [
            'A [dead]',
            'B Madrid',
            'C London',
        ])

    def test_eval_3(self):
        v = diplomacy_eval([
            'A Madrid Hold',
            'B Barcelona Move Madrid',
            'C London Support B',
            'D Austin Move London',
        ])
        self.assertEqual(v, [
            'A [dead]',
            'B [dead]',
            'C [dead]',
            'D [dead]',
        ])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [
            'A [dead]',
            'B Madrid',
            'C [dead]',
            'D Paris',
        ])
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB Madrid\nC [dead]\nD Paris\n')

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO(
            'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n')
    
    def test_solve_2(self):
        # intentional typo in city name
        r = StringIO(
            'A Madrid Hold\nB Barcelona Move Madri\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n')
        w = StringIO()
        with self.assertRaises(AssertionError):
            diplomacy_solve(r, w)
    
    def test_solve_3(self):
        # intentional typo in army name
        r = StringIO(
            'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support X\nE Austin Support A\n')
        w = StringIO()
        with self.assertRaises(AssertionError):
            diplomacy_solve(r, w)


if __name__ == '__main__':  # pragma: no cover
    unittest.main()
