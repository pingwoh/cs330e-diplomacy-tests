#!/usr/bin/env python3


# ------------------------------
# projects/diplomacy/TestDiplomacy.py
# ------------------------------
# -------
# imports
# -------
import sys
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_eval, diplomacy_print


# -----------
# TestCollatz
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_diplomacy_read1(self):
        inp = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid"
        op = diplomacy_read(inp)
        self.assertEqual(op, "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")

    def test_diplomacy_read2(self):
        inp = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B"
        op = diplomacy_read(inp)
        self.assertEqual(op, "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")

    def test_diplomacy_read3(self):
        inp = "A Madrid Hold"
        op = diplomacy_read(inp)
        self.assertEqual(op, "A Madrid Hold")

    # ----
    # eval
    # ----
    def test_diplomacy_eval1(self):
        inp = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid']]
        op = diplomacy_eval(inp)
        self.assertEqual(op, [['A [dead]'], ['B', '[dead]']])


    def test_diplomacy_eval2(self):
        inp = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B'], ['D', 'Austin', 'Move', 'London']]
        op = diplomacy_eval(inp)
        self.assertEqual(op, [['A [dead]'], ['B', '[dead]'], ['C [dead]'], ['D', '[dead]']])

    def test_diplomacy_eval3(self):
        inp = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid']]
        op = diplomacy_eval(inp)
        self.assertEqual(op, [['A [dead]'], ['B', '[dead]'], ['C', '[dead]']])

    # ----
    # print
    # ----
    def test_diplomacy_print1(self):
        inp = [['A [dead]'], ['B', '[dead]'], ['C', '[dead]']]
        op = diplomacy_print(inp,sys.stdout)
        self.assertEqual(op, "A [dead]\nB [dead]\nC [dead]\n")

    def test_diplomacy_print2(self):
        inp = [['A [dead]'], ['B', '[dead]'], ['C [dead]'], ['D', '[dead]']]
        op = diplomacy_print(inp, sys.stdout)
        self.assertEqual(op, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_diplomacy_print3(self):
        inp = [['A [dead]'], ['B', '[dead]'], ['C', '[dead]'], ['D', 'Paris'], ['E', 'Austin']]
        op = diplomacy_print(inp, sys.stdout)
        self.assertEqual(op, "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    # ----
    # solve
    # ----
    def test_diplomacy_solve1(self):
        inp = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B'], ['D', 'Austin', 'Move', 'London']]
        op  = diplomacy_solve(sys.stdout,inp)
        self.assertEqual(op, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_diplomacy_solve2(self):
        inp = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid']]
        op  = diplomacy_solve(sys.stdout,inp)
        self.assertEqual(op, "A [dead]\nB [dead]\nC [dead]\n")

    def test_diplomacy_solve3(self):
        inp = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B'], ['E', 'Austin', 'Support', 'A']]
        op  = diplomacy_solve(sys.stdout,inp)
        self.assertEqual(op, "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
# ----
# main
# ----
if __name__ == "__main__":
    main()

"""

............
----------------------------------------------------------------------
Ran 12 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          33      2     20      2    92%   9->exit, 48-49
TestDiplomacy.py      55      1      2      1    96%   101
--------------------------------------------------------------
TOTAL                 88      3     22      3    95%



"""