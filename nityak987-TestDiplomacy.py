# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_position, diplomacy_solve, diplomacy_print
# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


    def test_solve2(self):
        r = StringIO("A Madrid Move Paris\nB Paris Move Moscow\nC Jersey Support D\nD NYC Move Dallas\n E Dallas Hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A Paris\nB Moscow\nC Jersey\nD Dallas\nE [dead]\n")


    def test_solve3(self):
        r = StringIO("A Houston Hold\nB Boston Hold\nC L.A Move Boston\nD Honolulu Support B\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A Houston\nB Boston\nC [dead]\nD Honolulu\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()
