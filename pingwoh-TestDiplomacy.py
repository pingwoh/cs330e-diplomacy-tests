# -------
# imports
# -------

from io import StringIO
from sys import stdout
from unittest import main, TestCase

from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_solve, city, army

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    def create_file(self, *lines):
        self.newFile = open("unitTests.txt", "w")
        linesToWrite = ""
        for i in lines:
            linesToWrite += i
        self.newFile.write(linesToWrite)
        self.newFile.close()

        # pass created file back
        return self.newFile

    # ----
    # eval
    # ----
    def test_solve1(self):
        self.create_file("A Madrid Hold\n")
        file = open("unitTests.txt", "r")
        ans = diplomacy_eval(file)
        file.close()

        ansUnpacked = ""
        for play in ans:
            ansUnpacked += play.name + " " + play.status + "\n"
        self.assertEqual(ansUnpacked, "A Madrid\n")

    def test_solve2(self):
        self.create_file("A Madrid Move London\n")
        file = open("unitTests.txt", "r")
        ans = diplomacy_eval(file)
        file.close()

        ansUnpacked = ""
        for play in ans:
            ansUnpacked += play.name + " " + play.status + "\n"

        self.assertEqual(ansUnpacked, "A London\n")

    def test_solve3(self):
        self.create_file("A Madrid Hold\n", "B Barcelona Move Madrid\n")
        file = open("unitTests.txt", "r")
        ans = diplomacy_eval(file)
        file.close()

        ansUnpacked = ""
        for play in ans:
            ansUnpacked += play.name + " " + play.status + "\n"
        self.assertEqual(ansUnpacked, "A [dead]\nB [dead]\n")

    def test_solve4(self):
        self.create_file("A Madrid Move London\n", "B London Hold\n", "C Seoul Support A\n")
        file = open("unitTests.txt", "r")
        ans = diplomacy_eval(file)
        file.close()

        ansUnpacked = ""
        for play in ans:
            ansUnpacked += play.name + " " + play.status + "\n"
        self.assertEqual(ansUnpacked, "A London\nB [dead]\nC Seoul\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
